import React, { useEffect, useState } from "react";
import TableHeader from "../main/TableHeader";
import TableBody from "../main/TableBody";
import Spinner from "../main/Spinner";
import LazyLoad from "react-lazyload";

const AllRecipe = () => {
  const [sentState, setSentState] = useState(false); //

  const [tableData, setTableData] = useState([]);
  const API ="https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?page=1";

  useEffect(() => {
    fetch(API)
      .then((res) => res.json())
      .then((data) => {
        let { results } = data;
        setTableData(results);
      });
  }, []);


  let tableDataList = <tbody>{tableData.length?tableData.map(item =><TableBody checkedState={sentState} allRecipeDetail={item} key={item.id}/>):null}</tbody>
  // let tableDataList = <tbody>{tableData.length?tableData.map(item => <LazyLoad key={item.id} height={10} offset={10} placeholder={<Spinner />}><TableBody checkedState={sentState} allRecipeDetail={item} key={item.id}/></LazyLoad>):null}</tbody>

  const changeHandlerCheckbox = (val) => {
    setSentState(val);
  };

  return (
    <div className="">
        <LazyLoad once={true} height={1} offset={100} placeholder={<Spinner />}>
          <table>
            <TableHeader changeHandlerCheckbox={changeHandlerCheckbox} />
              {tableDataList}
          </table>
        </LazyLoad>
    </div>
  );
};

export default AllRecipe;
//   let tableDataList = <tbody>{tableData.length?tableData.map(item => <LazyLoad key={item.id} height={100} offset={[-100, 100]} placeholder={<Spinner />}><TableBody checkedState={sentState} allRecipeDetail={item} key={item.id}/></LazyLoad>):null}</tbody>
