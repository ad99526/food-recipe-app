import React, { useEffect, useState } from "react";
import TableHeader from "../main/TableHeader";
import TableBody from "../main/TableBody";
import Spinner from "../main/Spinner";
import LazyLoad from "react-lazyload";

const Disabled = () => {
  const [sentState, setSentState] = useState(false);//


  const [tableData, setTableData] = useState([]);
  const API = "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?id_disabled=true";
  
  useEffect(() => {
    fetch(API).then(res=>res.json()).then(data=>{
      let {results} = data;
      setTableData(results);
      console.log(results, "");
    })
  },[])

  let tableDataList = <tbody>{tableData.length?tableData.map(item => <TableBody checkedState={sentState} allRecipeDetail={item} key={item.id}/>):null}</tbody>


  const changeHandlerCheckbox=(val)=>setSentState(val)
  return (
    <div className="">
      <LazyLoad once={true} height={1} offset={100} placeholder={<Spinner />}>
        <table>
          <TableHeader changeHandlerCheckbox={changeHandlerCheckbox}/>
        {tableDataList}
        </table>
      </LazyLoad>
    </div>
  );
}
export default Disabled;