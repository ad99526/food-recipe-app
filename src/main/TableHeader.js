import React from "react";


const toggleIconStyle1 = {
  width: "20px",
  height: "20px",
  color: "black",
  display: "inline-block"
};

const toggleIconStyle2 = {
  width: "20px",
  height: "20px",
  color: "transparent",
  display: "inline-block"
};

const TableHeader = ({changeHandlerCheckbox}) => {
  const toggleOrderIcon = (
    <div className="toggle-icon" style={toggleIconStyle1}>
      <ion-icon name="chevron-up-outline"></ion-icon>
      <ion-icon name="chevron-down-outline"></ion-icon>
    </div>
  );
  const toggleOrderIconTrasnparent = (
    <div className="toggle-icon-transparent" style={toggleIconStyle2}>
      <ion-icon name="chevron-up-outline"></ion-icon>
      <ion-icon name="chevron-down-outline"></ion-icon>
    </div>
  );

// const toggleState

  const checkBox = (
    <label className="checkbox">
      <span className="checkbox__input">
        <input
          type="checkbox"
          name="checkbox"
          onChange={(e) => changeHandlerCheckbox(e.target.checked)}
        />
        <span className="checkbox__control" style={{ backgroundColor: "white" }}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            aria-hidden="true"
            focusable="false"
          >
            <path
              fill="none"
              stroke="currentColor"
              strokeWidth="3"
              d="M1.73 12.91l6.37 6.37L22.79 4.59"
            />
          </svg>
        </span>
      </span>
    </label>
  );
  return (
    <tr className="table-header">
      <th className="table-head">
        <span>{checkBox}</span>
      </th>
      <th className="table-head">NAME {toggleOrderIcon}</th>
      <th className="table-head">LAST UPDATE{toggleOrderIcon}</th>
      <th className="table-head">COGS {toggleOrderIcon}</th>
      <th className="table-head">COST PRICE( ) {toggleOrderIcon}</th>
      <th className="table-head">SALE PRICE {toggleOrderIcon}</th>
      <th className="table-head">GROSS MARGIN {toggleOrderIcon}</th>
      <th className="table-head">TAGS/ACTIONS{toggleOrderIconTrasnparent}</th>
    </tr>
  );
};

export default TableHeader;
