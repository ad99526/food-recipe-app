import React from "react"
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import IconContainer from "./IconContainer";

const HmgItem = ({recipeName="Ambur Biryani", percentageValue=80, type, colour}) => {
  
  const colourOfIcon = colour==="green"?"green":"red"

  return(
  <div className="hmg-item col-4-of-12">
      <p className="hmg-item-recipe">{recipeName}</p>
      <p className="circle">
        {type ?
          <CircularProgressbar
          styles={buildStyles({
            pathColor: colourOfIcon,
            textColor: colourOfIcon,
            trailColor: "#d6d6d63e",
            backgroundColor: "blue"
          })}
          value={percentageValue}
          text={`${percentageValue}%`}
          strokeWidth={5}
        /> :
        <span className="icon-container">
          <IconContainer percentageValue={percentageValue}/>
        </span>}
      </p>
  </div>)
}

export default HmgItem;

