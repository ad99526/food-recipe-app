import React from "react";

const IconContainer = ({ direction, percentageValue }) => {
  const  styling = {
    color: "green"
  }
  return (
    <span className="icon-container-1" style={styling}>
      {`${percentageValue}%`}
      <ion-icon name="arrow-up-outline"></ion-icon>
    </span>
  );
};

export default IconContainer;
