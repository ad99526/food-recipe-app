import React from "react";
import Container from "./Container";

const MainContainer = () => {

  return (
      <div className="container-1 row">
        <Container colour="green" heading={"High Margin Recipes"} marginOrder="top"/>
        <Container colour="red" heading={"Low Margin Recipes"} marginOrder="bottom"/>
        <Container colour="" heading={"Top Fluctuating Recipes"} fluctuationOrder="top"/>
      </div>
  );
};

export default MainContainer;