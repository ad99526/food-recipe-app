import React, { useState,useEffect } from "react";

const TableHeader = ({ allRecipeDetail, checkedState}) => {
  let lastdate = allRecipeDetail.last_updated.date.split(" ");
  let mr = Math.round;

  const [checkBoxState, setCheckBoxState] = useState(checkedState);

  useEffect(() => {
    setCheckBoxState(checkedState);
  },[checkedState])

  const onChangeCheckBoxState = (e) => {
  if(checkBoxState){
    setCheckBoxState(false);
  }else{setCheckBoxState(true)}
}
  let checkBox = (
    <label className="checkbox">
      <span className="checkbox__input">
        <input type="checkbox" name="checkbox" checked={checkBoxState}
          onChange={(e) => onChangeCheckBoxState(e)}
        />
        <span
          className="checkbox__control"
          style={{ backgroundColor: "white" }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            aria-hidden="true"
            focusable="false"
          >
            <path
              fill="none"
              stroke="currentColor"
              strokeWidth="3"
              d="M1.73 12.91l6.37 6.37L22.79 4.59"
            />
          </svg>
        </span>
      </span>
    </label>
  );
  return (
    <tr className="table-row">
      <td>
        <span>
          {checkBox}
        </span>
      </td>
      <td>{allRecipeDetail.name}</td>
      <td>{lastdate[0]}</td>
      <td>{allRecipeDetail.cogs} %</td>
      <td>{mr(allRecipeDetail.cost_price)}</td>
      <td>{mr(allRecipeDetail.sale_price)}</td>
      <td>{allRecipeDetail.gross_margin.toFixed(3)}</td>
      <td>
        <span className="tagname">Indian Made</span>
      </td>
    </tr>
  );
};

export default TableHeader;

// <input type="checkbox" id="cbx" className="css-checkbox"/>
// <label for="cbx" className="css-label"></label>
// onChange={() => setCheckedState(!checkedState)}