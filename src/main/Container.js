import React, { useEffect, useState } from "react";
import HmgItem from "./HmgItem";
import { nanoid } from "nanoid";
const Container = ({ heading = "Recipes", marginOrder, fluctuationOrder, colour }) => {

  const [recipeMargin, setRecipeMargin] = useState([]);
  const [parameter, setParameter] = useState("");

  const API_MARGIN = `https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/margin-group/?order=${marginOrder}`;
  const API_FLUCTUATION = `https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/fluctuation-group/?order=${fluctuationOrder}`;
  let API = marginOrder ? API_MARGIN : API_FLUCTUATION;

 
  useEffect(() => {
    fetch(API).then((res) => res.json())
    .then((data) => {
      setRecipeMargin(data.results);
    })
    .catch((err) => console.log("The error is" + err));
  }, [API]);

  const recipeDetailList = recipeMargin.map((item,i) => (
    <HmgItem
      type={item.margin ? "margin":""}
      recipeName={item.name}
      percentageValue={item.margin ? item.margin : item.fluctuation}
      colour={colour}
      key={nanoid()}
    />
  ));

  return (
    <div className="container-11 col-4-of-12">
      <h2>{heading}</h2>
      <div className="hmg-container row">{recipeDetailList}</div>
    </div>
  );
};

export default Container;
