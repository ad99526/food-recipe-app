import React, { useEffect, useState } from "react";

import MainContainer from "./main/MainContainer";
import MainTab from "./tabComponents/MainTab";

import "react-circular-progressbar/dist/styles.css";
import "react-tabs/style/react-tabs.css";
import "./styles.css";


const styling = {margin: "0 15px", backgroundColor: "transparent"};
export default function App() {
  
  return (
    <div className="app">
      <MainContainer />
      <div style={styling}>
        <MainTab/>
      </div>
    </div>
  );
}
