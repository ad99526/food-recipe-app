import React from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import AllRecipe from "../components/AllRecipe";
import Disabled from "../components/Disabled";
import Incorrect from "../components/Incorrect";
import Untagged from "../components/Untagged";
const MainTab = () => {
  const styling = {
    textTransform: "uppercase",
    fontWeight: "600"
  }

  return (
    <Tabs defaultFocus={false}>
      <TabList>
        <Tab><h2 style={styling}>All Recipes</h2></Tab>
        <Tab><h2 style={styling}>Incorrect</h2></Tab>
        <Tab><h2 style={styling}>Untagged</h2></Tab>
        <Tab><h2 style={styling}>Disabled</h2></Tab>
      </TabList>

      <TabPanel>
        <AllRecipe/>
      </TabPanel>
      <TabPanel>
        <Incorrect/>
      </TabPanel>
      <TabPanel>
        <Untagged/>
      </TabPanel>
      <TabPanel>
        <Disabled/>
      </TabPanel>
    </Tabs>
  );
};

export default MainTab;
